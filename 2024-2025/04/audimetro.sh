#!/bin/bash

# IDEAS PARA MEJORAR EL AUDIMETRO:
#
# - Hacer dos pasadas una por cada oido (explorar de qué forma se
#   puede indicar a play que saque el sonido por el canal derecho o
#   izquierdo)

# Lista de frecuencias a probar (en Hz)
FREQUENCIES=(25 50 100 160 250 500 1000 1500 2000 3000 4000 6000 8000 10000)

# Función para generar sonido en una frecuencia con un volumen específico
generate_sound() {
    local frequency=$1
    local gain=$2
    play -n synth 1 sin $frequency vol 1 gain $gain &> /dev/null &
}

# Función para hacer la prueba en una frecuencia específica
test_frequency() {
    local frequency=$1
    GAIN=-100

    echo "Probando frecuencia ${frequency}Hz. Presiona cualquier tecla cuando lo escuches."
    
    while true; do
        # Generar el sonido
        generate_sound $frequency $GAIN
        
        # Leer la respuesta del usuario con un timeout (si no pulsa nada en 1 segundo, sigue)
        read -t 1.2 -n 1 key
        
        # Si el usuario presiona una tecla, sale del bucle
        if [ $? -eq 0 ]; then
            echo "Escuchaste la frecuencia ${frequency}Hz con ganancia ${GAIN}dB"
            echo "$frequency,$GAIN" >> audiometro_resultados.csv
            break
        fi
        
        # Incrementar el volumen
        GAIN=$(echo "$GAIN + 5" | bc)
        
        # Limitar el volumen a un máximo de 1.0 (100%)
        if (( $(echo "$GAIN > 0" | bc -l) )); then
            echo "No se escuchó la frecuencia ${frequency}Hz incluso con el volumen máximo."
            echo "$frequency,No detectado" >> audiometro_resultados.csv
            break
        fi
    done
}

# Probar cada frecuencia
for frequency in "${FREQUENCIES[@]}"; do
    test_frequency $frequency
done

echo "Prueba completada. Resultados guardados en audiometro_resultados.csv."
