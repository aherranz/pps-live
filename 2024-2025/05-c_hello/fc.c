#include <stdio.h>
#include <stdlib.h>

int fahrenheit_a_celsius(int f) {
  /* CUIDADO CON LA ARITMÉTICA ENTERA */
  /* return (5 / 9) * (f - 32); */
  return 5 * (f - 32) / 9;
}

void imprimir_tabla(int min, int max, int paso) {
  int f;
  for (f = min; f <= max; f += paso) {
    printf("%d\t%d\n", f, fahrenheit_a_celsius(f));
  }  
}

int main(int argc, char *argv[]) {
  /* TODO: proteger el código para comprobar que los parámetros son
   *     "adecuados" (3 y entéros min max y paso, min < max, paso <=
   *     (max - min) y paso > 0), si no lo son hay que imprimir un
   *     mensaje de uso por la salida de error y terminar "mal".
   *
   * NOTA: para comprobar que son enteros tienes que usar strtol en
   * vez de atoi, consulta el manual.
   */
  
  imprimir_tabla(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));
  return 0;
}
