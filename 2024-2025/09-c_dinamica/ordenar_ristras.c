#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

void intercambiar(int *x, int *y) {
  int temp = *x;
  *x = *y;
  *y = temp;
}

void leer_y_ordenar(int n) {
  int i, j, *datos;

  /* Solicitar memoria para n enteros y almacenar el puntero en datos */
  datos = (int *) malloc(n * sizeof(int));

  /* Leer los enteros de la entrada estándar y almacenarlos en el "array" datos */
  for (i = 0; i < n; i++)
    scanf("%d", &datos[i]);

  /* Bubblesort */
  for (i = 0 ; i < n - 1; i++)
    for (j = 0 ; j < n - i - 1; j++)
      if (datos[j] > datos[j + 1])
        intercambiar(&datos[j], &datos[j+1]);

  /* Imprimir los enteros ordenados */
  for (i = 0; i < n; i++)
    printf("%d\n", datos[i]);

  /* Tenemos el compromiso de liberar la memoria que ya no vamos a usar */
  free(datos);
}

int main() {  
  int n = -1;

  scanf("%d", &n);
  while (n > 0) {
    printf("\n");
    leer_y_ordenar(n);
    n = -1;
    scanf("%d", &n);
  }
  
  return 0;
}
