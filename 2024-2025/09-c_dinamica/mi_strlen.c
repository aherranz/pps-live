#include <stddef.h>
#include <stdio.h>

size_t mi_strlen(char *s) {
  int n = 0;
  while (*s++) n++;
  return n;
}

int main() {
  char *s = "hola";
  printf("La long de s es %lu\n", mi_strlen(s));
  return 0;
}
