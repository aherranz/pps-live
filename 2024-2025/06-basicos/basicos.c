#include <stdio.h>
#include <limits.h>

/* x es global */
int x = 42;

int main() {
  char c;

  /* l es global */
  int l = 37;
  
  /* Nota: el tipo que devuelve sizeof es size_t que internamente es
     un long unsgined int por eso necesitamos el conversor %lu */
  printf("Tamaño de c es %lu bytes\n", sizeof(c));

  printf("Contenido de la variable x: %i\n", x);
  printf("La dirección de memoria en la que están los datos de x es %p\n", &x);

  printf("Contenido de la variable l: %i\n", l);
  printf("La dirección de memoria en la que están los datos de l es %p\n", &l);

  printf("Tamaño de la dirección de memoria de x: %lu bytes\n", sizeof(&x));

  printf("Tamaño de un unsigned int: %lu bytes\n", sizeof(unsigned int));
  printf("Valor máximo de unsigned int: %u\n", UINT_MAX);
  return 0;
}
