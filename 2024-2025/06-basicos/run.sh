#!/bin/bash

# CFLAGS="-Wall -Werror -ansi -pedantic"
CFLAGS="-Wall -Werror -ansi"
if gcc ${CFLAGS} -o $1 $1.c; then
  ./$1
fi
