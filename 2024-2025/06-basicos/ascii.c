#include <stdio.h>
#include <ctype.h>
#include <limits.h>

int main() {
        printf("\033[31mA \033[33mS \033[32mC \033[36mI \033[34mI\033[0m\n");

        int i;
        for (i = 0; i < UCHAR_MAX; i++) {
                if (i % 5 == 0) printf("\n");

                if (isprint(i)) {
                        printf("\033[32m'%c' (0x%2x)\t", i, i);
                } else {
                        printf("\033[31m'.' (0x%2x)\t", i);
                }
        }

        printf("\033[0m\n");
        return 0;
}
