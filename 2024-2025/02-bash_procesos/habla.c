#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  char *s;
  char buffer[2049];
  FILE *fd;
  
  /* (1) */
  for (int i = 0;  i < argc; i++) {
    fprintf(stderr, "Argumento %d: %s\n", i, argv[i]);
  }

  /* (2) */
  fgets(buffer, 2048, stdin);
  printf("Línea de la entrada estándar: %s\n", buffer);

  /* (3) */
  fd = fopen("habla.txt", "r");
  if (fd == NULL) {
    fprintf(stderr, "El fichero no existe\n");
    exit(-1);
  }  
  fgets(buffer, 2048, fd);  
  printf("Línea del fichero habla.txt: %s\n", buffer);
  
  /* (4) */
  s = getenv("VARHABLA");
  printf("El valor de la variable VARHABLA es %s\n", s);

  /* (5) */
  return argc - 1;
}


