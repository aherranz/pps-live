#include <stdio.h>

int a[10];
int b[10];

/* Función ilen */
long unsigned ilen(int x[]) {
  /* IMPORTANTE: C pierde información de tamaños al pasar un array como argumento! */
  /* De hecho interpreta int x[] como si fuera int *x */
  /* ¡¡¡DESASTRE!!! (el tamaño de x es 8) */
  return sizeof(x) / sizeof(x[0]);
}

int main() {
  /* Las variables de tipo array son inmutables */
  /* b = a; */
  printf("Longitud de a es %lu\n", ilen(b));
  return 0;
}
