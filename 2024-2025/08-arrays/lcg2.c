#include <stdio.h>
#include <stddef.h>

#define A 7
#define C 1
#define M 11

int x = 0;

int generar_aleatorio() {
  int aleatorio = x;
  x = (A * x + C) % M;
  return aleatorio;
}

/* Convención entre lo programadores de C: se pasa la longitud del array en otro parámetro */
int media(int a[], size_t n) {
  size_t i;
  int sum = 0;
  for (i = 0; i < n; i++)
    sum += a[i];
  
  return sum / n;
}

int main() {
  int i;
  int aleatorio[M];

  for (i = 0; i < M; i++)
    aleatorio[i] = generar_aleatorio();

  /* Nos salimos del array por delante y por detrás para ver qué datos "hay" */
  for (i = -M; i <= M; i++)
    printf("%i -> %i\n", i, aleatorio[i]);

  /* Supongamos que no conozco el tipo de los elementos del array aleatorio */
  /* ¿Cómo puedo saber su longitud? */
  printf("sizeof(aleatorio) == %lu\n", sizeof(aleatorio));
  printf("sizeof(aleatorio[0]) == %lu\n", sizeof(aleatorio[0]));
  printf("Longitud del array aleatorio == %lu\n",
         sizeof(aleatorio) / sizeof(aleatorio[0]));

  printf("La media del generador es %i\n",
         media(aleatorio, M));
  
  return 0;
}
