#include <stdio.h>
#include <string.h>

int main() {
  char s[] = "mundo";
  printf("El STRING \"s\" es \"%s\"\n", s);

  s[2] = '\0';
  printf("El STRING \"s\" es \"%s\"\n", s);
  printf("Long del ARRAY \"s\" es %lu\n",
         sizeof(s) / sizeof(s[0]));
  printf("Long del STRING \"s\" es %lu\n", strlen(s));

  /* Reponemos la n */
  s[2] = 'n';
  /* Nos cargamos el \0 de terminación y exploramos */
  s[5] = '_';
  /* s[6] = '_'; */
  printf("El STRING \"s\" es \"%s\"\n", s);
  
  return 0;
}
