#include <stdlib.h>
#include <stdio.h>

size_t slen(char s[]) {
  int n = 0;
  while(s[n]) n++;
  return n;
}

int main() {
  int i;
  char s[] = "mundo";
  s[3] = 0;
  printf("El string es \"%s\"\n", s);
  printf("La longitud del array s es %lu\n",
         sizeof(s) / sizeof(s[0]));
  printf("La longitud del string es %lu\n", slen(s));
  for (i = 0; i < 6; i++) {
    printf("%d\n",s[i]);
  }
  return 0;
}
