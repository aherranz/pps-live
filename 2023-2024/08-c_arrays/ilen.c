#include <stdio.h>

size_t ilen(int a[]) {
  return sizeof(a) / sizeof(int);
}

int main() {
  int x[100];
  printf("La long de x es %lu\n", ilen(x));
  return 0;
}
  
