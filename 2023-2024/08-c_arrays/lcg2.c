#include <stdio.h>

#define A 7
#define C 1
#define M 11

int x = 0;

int generar_aleatorio() {
  int anterior = x;
  x = (A * x + C) % M;
  return anterior;
}

int main()
{
  int i;
  int aleatorio[M];

  for (i = 0; i < M; i++) {
    aleatorio[i] = generar_aleatorio();
  }
  
  for (i = -M; i <= 1000 * M; i++) {
    printf("%i -> %i\n", i, aleatorio[i]);
  }
  
  return 0;
}
