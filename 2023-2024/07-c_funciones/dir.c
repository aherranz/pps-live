#include <stdio.h>

int main() {
  int *p;
  int x;
  int y;

  x = 42;
  p = &x;
  printf("El valor en x es                %d\n", x);
  printf("La dirección de memoria de x es %p\n", &x);
  printf("El valor de p es                %p\n", p);
  printf("El tamaño en bytes de p es      %lu\n", sizeof(p));
  printf("El tamaño en bytes de &x es     %lu\n", sizeof(&x));
  printf("El valor en la dirección %p es %d\n", p, *p);

  y = x;
  printf("El valor en y es                %d\n", y);
  printf("La dirección de memoria de y es %p\n", &y);
  
  x = 27;
  printf("El valor en x es                %d\n", x);
  printf("La dirección de memoria de x es %p\n", &x);
  printf("El valor de p es                %p\n", p);
  printf("El valor en la dirección %p es %d\n", p, *p);

  printf("El valor en y es                %d\n", y);
  printf("La dirección de memoria de y es %p\n", &y);

  *p = 0;
  printf("El valor en x es                %d\n", x);
  printf("La dirección de memoria de x es %p\n", &x);
  printf("El valor de p es                %p\n", p);
  printf("El valor en la dirección %p es %d\n", p, *p);
  
  return 0;
}
