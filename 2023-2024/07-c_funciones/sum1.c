#include <stdio.h>

unsigned sum(unsigned i) {
  if (i < 1) {
    return 0;
  }
  else {
    return i + sum(i-1);
  }
}

int main() {
  unsigned n = 500000;
  printf("0+1+...+%u = %u\n ", n, sum(n));
  
  return 0;
}
