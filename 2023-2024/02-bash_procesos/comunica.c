#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
  int i;
  char buffer[2048];
  char *s;
  FILE *fd;
  
  /* (1) */
  for (i = 0; i < argc; i++) {
    fprintf(stderr,"Argumento %i: \"%s\"\n",i,argv[i]);
  }

  /* (2) */
  fgets(buffer, 2048, stdin);
  fprintf(stdout, "Primera línea de stdin:\n%s", buffer);
  
  /* (3) */
  fd = fopen("comunica.txt", "r");
  if (fd == NULL) {
    fprintf(stderr, "ERROR: el fichero comunica.txt no existe o no se puede abrir, corrígelo y vuelve a ejecutar\n");
    exit(1);
  }
  else {
    fgets(buffer, 2048, fd);
    fprintf(stdout, "Primera línea de comunica.txt:\n%s", buffer);
  }
  
  /* (4) */
  s = getenv("MICOMUNICA");
  if (s == NULL) {
    fprintf(stderr, "ERROR: la variable MICOMUNICA no está definida, defínela y vuelve a ejecutar\n");
    exit(1);
  }
  else {
    fprintf(stdout, "Valor de variable de entorno MICOMUNICA: \"%s\"\n", s);
  }

  /* (5) */
  return argc - 1;
}
