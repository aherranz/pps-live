#include <stdio.h>
#include <stdlib.h>

int fahrenheit_a_celsius(int f) {
  return 5 * (f - 32) / 9;
}

void imprimir_tabla(int min, int max, int paso) {
  int f;
  printf("F\tC\n");
  for(f = min; f <= max; f += paso) {
    printf("%d\t%d\n", f, fahrenheit_a_celsius(f));
  }
}

int main(int argc, char *argv[]) {
  int min, max;
  int paso;
  if (argc != 4) {
    fprintf(stderr, "USO: ./fc MIN MAX PASO\n");
    return 1;
  }
  min = atoi(argv[1]);
  max = atoi(argv[2]);
  paso = atoi(argv[3]);
  if (max < min) {
    fprintf(stderr, "USO: ./fc MIN MAX PASO\n");
    fprintf(stderr, "  MAX no puede ser menor que MIN\n");
    return 1;
  }
  if (paso <= 0) {
    fprintf(stderr, "USO: ./fc MIN MAX PASO\n");
    fprintf(stderr, "  PASO debe ser mayor que 0\n");
    return 1;
  }
  imprimir_tabla(min, max, paso);
  return 0;
}
