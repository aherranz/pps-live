#include <stdio.h>
#include <ctype.h>
#include <string.h>

enum mes_e {
  ENERO,
  FEBRERO,
  MARZO,
  ABRIL,
  MAYO,
  JUNIO,
  JULIO,
  AGOSTO,
  SEPTIEMBRE,
  OCTUBRE,
  NOVIEMBRE,
  DICIEMBRE
};

int main(int argc, char *argv[]) {
  unsigned int i;
  enum mes_e m;

  /* Transforma argv[1] a minusculas */  
  for (i = 0; i < strlen(argv[1]); i++)
    argv[1][i] = tolower(argv[1][i]);

  /* SI argv[1] es "enero" IMPRIME el valor de ENERO */
  if (strcmp(argv[1], "enero") == 0) {
    printf("%d\n", ENERO);
  }
  
  
  
}
