#include <stdio.h>
#include <stdlib.h>

double rand01() {
  return (double) rand() / RAND_MAX;
}

int main(int argc, char *argv[]) {
  long f, c;
  long n, m;
  double x;

  /* TODO: comprobar uso correcto (argc, y n y m > 0) */
  n = atol(argv[1]);
  m = atol(argv[2]);

  printf("%li\n", n);
  printf("%li\n", m);

  srand(42);
  
  for (f = 0; f < n; f++) {
    x = rand01();
    printf("%.2f", x);
    for (c = 1; c < m; c++) {
      x = rand01();
      printf(" %.2f", x);
    }
    printf("\n");
  }
  return 0;
}
