#!/bin/bash

PROGRAMA="basicos"
CFLAGS="-Wall -Wextra -Werror -ansi"

if gcc $CFLAGS -o $PROGRAMA $PROGRAMA.c; then
    ./$PROGRAMA
    exit 0
else
    exit 1
fi    
