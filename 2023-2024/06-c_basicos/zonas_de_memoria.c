#include <stdio.h>
#include <stdlib.h>

int g1;
int *g2;
int g3[100];

void f() {
  int l1;
  int *l2;
  int l3[100];
  l2 = malloc(1);
  printf("f\t&l1\t%p\n", &l1);
  printf("f\t&l2\t%p\n", &l2);
  printf("f\t&l3\t%p\n", &l3);
  printf("f\tl3\t%p\n", l3);
  printf("f\tl2\t%p\n", l2);
}
    
int main() {
  int l1;
  int *l2;
  int l3[100];
  g2 = malloc(1);
  l2 = malloc(1);
  printf("main\t&f\t%p\n", &f);
  printf("main\t&main\t%p\n", &main);
  printf("main\t&g1\t%p\n", &g1);
  printf("main\t&g2\t%p\n", &g2);
  printf("main\t&g3\t%p\n", &g3);
  printf("main\tg2\t%p\n", g2);
  printf("main\tg3\t%p\n", g3);
  printf("main\t&l1\t%p\n", &l1);
  printf("main\t&l2\t%p\n", &l2);
  printf("main\t&l3\t%p\n", &l3);
  printf("main\tl3\t%p\n", l3);
  printf("main\tl2\t%p\n", l2);
  f();
  return 0;
}
