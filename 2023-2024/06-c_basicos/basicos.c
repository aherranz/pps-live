#include <stdio.h>

char gc = 'j';
short gs = 1031;
int gi = -42;
float gf = 0.21;
double gd = 0.33;

void f() {
  char fc;
  int fi;

  printf("Dirección de memoria de fc: %p\n", &fc);
  printf("Dirección de memoria de fi: %p\n", &fi);

}


void m() {
  printf("Tamaño en bytes de unsigned char: %lu\n", sizeof(unsigned char));
  printf("Tamaño en bytes de unsigned: %lu\n", sizeof(unsigned));
  printf("Tamaño en bytes de unsigned int: %lu\n", sizeof(unsigned int));
  printf("Tamaño en bytes de short: %lu\n", sizeof(short));
  printf("Tamaño en bytes de short: %lu\n", sizeof(short int));
  printf("Tamaño en bytes de long: %lu\n", sizeof(long));
  printf("Tamaño en bytes de long int: %lu\n", sizeof(long int));
  printf("Tamaño en bytes de long unsigned int: %lu\n", sizeof(long unsigned int));
  printf("Tamaño en bytes de long long: %lu\n", sizeof(long long));
  printf("Tamaño en bytes de long long int: %lu\n", sizeof(long long int));
  printf("Tamaño en bytes de long long unsigned: %lu\n", sizeof(long long unsigned));
  printf("Tamaño en bytes de long long unsigned int: %lu\n", sizeof(long long unsigned int));
  printf("Tamaño en bytes de long double: %lu\n", sizeof(long double));
}


int main() {
  char mc;
  int mi;
  printf("Tamaño en bytes de global char: %lu\n", sizeof(gc));
  printf("Tamaño en bytes de global short: %lu\n", sizeof(gs));
  printf("Tamaño en bytes de global int: %lu\n", sizeof(gi));
  printf("Tamaño en bytes de global float: %lu\n", sizeof(gf));
  printf("Tamaño en bytes de global double: %lu\n", sizeof(gd));
  printf("Tamaño en bytes de local char: %lu\n", sizeof(mc));
  printf("Tamaño en bytes de local int: %lu\n", sizeof(mi));
  printf("El valor de gc (como char): %c\n", gc);
  printf("El valor de gc (como entero): %d\n", gc);
  printf("El valor de gs (como entero): %d\n", gs);
  printf("El valor de gs (como char): %c\n", gs);
  printf("El valor de gi: %i\n", gi);
  printf("El valor de gf: %f\n", gf);
  printf("El valor de gd: %f\n", gd);
  
  printf("Dirección de memoria de gc: %p\n", &gc);
  printf("Dirección de memoria de gs: %p\n", &gs);
  printf("Dirección de memoria de gi: %p\n", &gi);
  printf("Dirección de memoria de gf: %p\n", &gf);
  printf("Dirección de memoria de gd: %p\n", &gd);
  
  printf("Dirección de memoria de mc: %p\n", &mc);
  printf("Dirección de memoria de mi: %p\n", &mi);

  f();

  printf("Dirección de memoria de la función main: %p\n", &main);
  printf("Dirección de memoria de la función f: %p\n", &f);


  m();
  return 0;
}
