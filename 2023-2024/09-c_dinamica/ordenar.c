#include <stdio.h>
#include <stdlib.h>

int n;
int *datos;

void intercambiar(int *x, int *y) {
  int aux = *x;
  *x = *y;
  *y = aux;
}

void ordenar() {
  int i, j;
  for (i = 0; i < n - 1; i++)
    for (j = 0; j < n - 1; j++)
      if (datos[j] > datos[j+1])
        intercambiar(&datos[j], &datos[j+1]);
}

int main() {
  int i;
  scanf("%d",&n);
  printf("Número de enteros: %d\n", n);

  datos = (int *)malloc(n * sizeof(int));
  
  for (i = 0; i < n; i++) {
    scanf("%d", &datos[i]);
  }

  ordenar();
  
  for (i = 0; i < n; i++) {
    printf("%d\n", datos[i]);
  }

  free(datos);
  
  return 0;
}
 
