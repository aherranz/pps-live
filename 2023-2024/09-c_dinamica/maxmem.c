#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

int main() {
  void *p;
  size_t n;
  size_t n_min = 0;
  size_t n_max = SIZE_MAX;
  while(n_min != n_max - 1) {
    n = n_min + (n_max - n_min) / 2;
    p = malloc(n);
    if (p != NULL) {
      free(p);
      fprintf(stderr, "%20lu EXITO\n", n);
      n_min = n;
    }
    else {
      fprintf(stderr, "%20lu FALLO\n", n);
      n_max = n;
    }
  }
  printf("%lu (B)\n", n);
  printf("%lu (KB)\n", n >> 10);
  printf("%lu (MB)\n", n >> 20);
  printf("%.2f (GB)\n", (double) n / (1024.0 * 1024.0 * 1024.0));
  return 0;
}
