#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

size_t longstr(char s[]) {
    size_t i = 0;
    while(s[i] != '\0') {
      i++;
    }
    return i;
}

int main() {
  int i;
  char mistring1[] = "mundo";
  char mistring2[6];
  mistring2[0] = 'm';
  mistring2[1] = 'u';
  mistring2[2] = '\0';
  mistring2[3] = 'd';
  mistring2[4] = 'o';
  mistring2[5] = '\0';
  /* Correcto: mistring2[5] = 0; */
  /* Incorrecto: mistring2[5] = '0' */;
  size_t n1 = sizeof(mistring1) / sizeof(mistring1[0]);
  size_t n2 = sizeof(mistring1) / sizeof(mistring1[0]);
  printf("El string mistring1 es \"%s\"\n", mistring1);
  printf("El string mistring2 es \"%s\"\n", mistring2);
  printf("La longitud del array mistring1 es %lu\n", n1);
  printf("La longitud del array mistring2 es %lu\n", n2);
  for (i = 0; i < n1; i++) {
    printf("El caracter %i es %c\n", i, mistring1[i]);
  }
  for (i = 0; i < n2; i++) {
    printf("El caracter %i es %c\n", i, mistring2[i]);
  }
  printf("La longitud del STRING mistring1 es %lu\n",
         longstr(mistring1));
  printf("La longitud del STRING mistring2 es %lu\n",
         longstr(mistring2));

  char mistring3[] = "angel";
  int j = atoi(mistring3);
  printf("El entero es %i\n", j);

  return 0;
}
