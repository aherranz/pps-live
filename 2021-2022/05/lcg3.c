#include <stdio.h>
#include <stddef.h>
#include "generar_lcg.h"

/* NO FUNCIONA LA IDEA DE sizeof(a) / sizeof(a[0]) VER TRANSPARENCIAS */
void imprimir_array(int a[], size_t n) {
  printf("La longitud de a %lu\n",
         sizeof(a) / sizeof(a[0]));
}

int main() {
  int i;
  int aleatorio[M];
  for (i=0; i < M; i++) {
    aleatorio[i] = generar_aleatorio();
  }
  for (i=0; i < M; i++) {
    printf("%i -> %i\n", i, aleatorio[i]);
  }

  imprimir_longitud(aleatorio);

  return 0;
}
