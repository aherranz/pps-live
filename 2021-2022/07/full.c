#include <stdlib.h>
#include <stdio.h>

#include <stdio.h>
#include <stdlib.h>


int main()
{
  int *p;
  size_t i = 1;

  p = (int *)malloc(i);
  while (p != NULL) {
    free(p);
    i *= 2;
    p = (int *) malloc(i);
    printf("Solicitados %lu bytes\n", i);
  }

  printf("Falló para %lu Mbytes\n", i / 1024U / 1024U);

  return 0;
}
