#include <stdlib.h>
#include <stdio.h>

void intercambiar(int *a, int *b)
{
   int aux=*a;
   *a=*b;
   *b=aux;
}

void print_array(int *arr,int size)
{
    int i;
    for(i=0;i<size;i++)
    {
        printf("%i\n",arr[i]);
    }
}


int main()
{
    int i,j,k,n,*datos;
    scanf("%d",&n);

    datos = (int *) malloc(n*sizeof(int));



    i=0;
    while (i<n) {
        scanf("%i",&datos[i]);
        i++;
    }

    for(j=0;j<n-1;j++) {
       for(k=0;k<n-j-1;k++) {
           if(datos[k]>datos[k+1]) {
               intercambiar(&datos[k],&datos[k+1]);
           }
       }
    }

    print_array(datos, n);

    free(datos);

    return 0;
}
