#include <stdio.h>
#include <stdlib.h>

int main() {
  int x, *p;
  long long int y, *q;

  x = 1;
  y = 2;
  p = &x;
  q = &y;
  printf("p == %p\n", p);
  printf("q == %p\n", q);
  p++;
  q++;
  printf("p == %p\n", p);
  printf("q == %p\n", q);
  return 0;
}
