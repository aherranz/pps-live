#include <stdio.h>

void intercambiar(int *a, int *b) {
  int aux = *a;
  *a = *b;
  *b = aux;
}

int main() {
  int x = 42, y = 27;
  printf("Antes de intercambiar: (%i, %i)\n", x, y);
  intercambiar(&x, &y);
  printf("Despues de intercambiar: (%i, %i)\n", x, y);
  return 0;
}
