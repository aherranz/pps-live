#include <stdio.h>
#include <stdlib.h>

struct {
  int x;
  int y;
} a, b;

struct punto {int x; int y;};
struct rectangulo {struct punto so; struct punto ne;};

int main() {
  a.x = 1;
  printf("Escribe el valor de a.y...\n");
  scanf("%i", &a.y);
  printf("a.x == %i\n", a.x);
  printf("a.y == %i\n", a.y);
  printf("b.x == %i\n", b.x);
  printf("b.y == %i\n", b.y);
  printf("Tamaño de a: %lu\n", sizeof(a));
  b = a;
  printf("a.x == %i\n", a.x);
  printf("a.y == %i\n", a.y);
  printf("b.x == %i\n", b.x);
  printf("b.y == %i\n", b.y);
  a.x = 2;
  printf("a.x == %i\n", a.x);
  printf("a.y == %i\n", a.y);
  printf("b.x == %i\n", b.x);
  printf("b.y == %i\n", b.y);


  struct rectangulo *p;
  p = (struct rectangulo *)malloc(sizeof(struct rectangulo));
  /* ERROR SINTAXIS: *p.ne.x = 42; */

  (*p).ne.x = 42;
  p->ne.x = 42; /* IDENTICO A LO ANTERIOR PERO MÁS BONITO */


  return 0;
}
