#!/bin/bash

# Para que este script funcione es necesario tener instalados los
# programas ./youtube-dl (instrucciones en la Web) y ffmpeg (con
# apt-get).

if test $# -ne 1; then
   echo "USO: ./yourmp3.sh URL (URL con video de youtube)" 1>&2
   exit 1;
fi;


########################################################################
# DESCARGAR EL VIDEO

# Preguntar a youtube el nombre del fichero que se va a descargar
NOMBRE_DE_FICHERO_VIDEO=$(./youtube-dl --get-filename "$1" 2> /dev/null)

# Otra idea para sacar el nombre del fichero ha sido esta (después de
# descargar el video):
# NOMBRE_DE_FICHERO_VIDEO=$(ls -t | head -1)
# pero no funciona pq el último fichero modificado puede no ser el
# video descargado.

# Si la URL no es un video entonces la salida estándar de
# youtube-dl --get-filename está vacía
if test -z "$NOMBRE_DE_FICHERO_VIDEO" ; then
    echo "ERROR DE URL" 1>&2
    exit 2
fi

# TODO: controlar si la descarga ha ido bien
./youtube-dl "$1"

########################################################################
# TRANSFORMAR VIDEO A MP3

# TODO: sería estupendo que el nombre del MP3 sea el mismo que el del
# video pero con .mp3 al final
NOMBRE_DE_FICHERO_MP3=mifichero.mp3

# TODO: controlar si la transformación ha ido bien
ffmpeg -i "$NOMBRE_DE_FICHERO_VIDEO" \
       -vn -ab 128k -ar 44100 -y \
       "$NOMBRE_DE_FICHERO_MP3"

########################################################################
# BORRAR FICHEROS INSERVIBLES

# TODO: asegurarse de que no hay errores en el borrado
rm $NOMBRE_DE_FICHERO_VIDEO

exit 0
