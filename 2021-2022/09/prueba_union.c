#include <stdio.h>
#include <string.h>

union {
  char telefono[16];
  char email[31];
} c;

int main() {
  printf("sizeof(c) == %lu\n", sizeof(c));
  strcpy(c.telefono, "34123456789");
  strcpy(c.email, "johndoe@example.org");
  printf("telefono == %s\n", c.telefono);
  printf("email == %s\n", c.email);
  printf("sizeof(c) == %lu\n", sizeof(c));
  return 0;
}
