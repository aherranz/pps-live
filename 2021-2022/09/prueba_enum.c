#include <ctype.h>
#include <stdio.h>
#include <string.h>

enum mes {ENERO = 1, FEBRERO, MARZO, ABRIL, MAYO, JUNIO, JULIO, AGOSTO, SEPTIEMBRE, OCTUBRE, NOVIEMBRE, DICIEMBRE};

char *meses_es[] =
  {"enero",
   "febrero",
   "marzo",
   "abril",
   "mayo",
   "junio",
   "julio",
   "agosto",
   "septiembre",
   "octubre",
   "noviembre",
   "diciembre"};

int main() {
  char nombre[20];
  unsigned i;
  enum mes m;

  scanf("%s", nombre);
  for (i = 0; i < strlen(nombre); i++) {
    nombre[i] = tolower(nombre[i]);
  }

  i = 0;
  while (strcmp(nombre, meses_es[i]) != 0
         && i < sizeof(meses_es)/sizeof(char *)) {
    i++;
  }
  m = ENERO + i;

  printf("MES: %d\n", m);
  return 0;
}
