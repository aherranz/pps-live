
#include <stdio.h>

/* Lee un array desde la entrada estandar
   Se supone que el array apuntado por a es capaz
   de guardar los datos */
int leer(int *a) {
    int tam, i;
    scanf("%d", &tam);
    for ( i = 0; i < tam; ++i) {
        /*
        int x;
        scanf("%d", &x);
        a[i] = x;*/
        scanf("%d", a+i);
    }
    return tam;
}

void escribir(int *a, int tam) {
    int i;
    for ( i = 0; i < tam; ++i ) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

int main()
{
    int v[32];
    int tam = leer(v);
    escribir(v, tam);
    escribir(v+1, 3);
    tam += leer(v+tam);
    escribir(v, tam);
    return 0;
}
