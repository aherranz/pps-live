
#include <stdio.h>

void escribir(int matriz[][3], int nFilas) {
    int i, j;
    for ( i = 0; i < nFilas; ++i ) {
        for ( j = 0; j < 3; ++j ) {
            printf("%d ", matriz[i][j]);
        }
        printf("\n");
    }
}

void escribirConPuntero(int (*matriz)[3], int nFilas) {
    int i, j;
    for ( i = 0; i < nFilas; ++i ) {
        for ( j = 0; j < 3; ++j ) {
            printf("%d ", matriz[i][j]);
        }
        printf("\n");
    }
}

/* int main(int argc, char *argv[])
int main(int argc, char **argv)
*/

int main()
{
    int matriz[][3] = {
        { 1, 2, 3 },
        { 4, 5 },
        { 7, 8, 9 },
        { 10, 11, 12}
    };
    printf("matriz[1,5] = %d\n", matriz[1][5]);
    escribir(matriz, 4);
    printf("sin la primera fila\n");
    escribir(matriz+1, 3);
    return 0;
}
