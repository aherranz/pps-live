#include <stdio.h>

int main(int argc, char *argv[]) {
  int j;

  if (argc == 1) {
    printf("Hola mundo\n");
  }
  else {
    for (j = 1; j < argc; j++) {
      printf("Hola %s\n", argv[j]);
    }
  }

  return 0;
}
