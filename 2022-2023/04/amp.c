#include <stdio.h>

int main() {
  int x = 42;
  int *p;

  p = &x;

  printf("El valor de *p es %i\n", *p);
  *p = 27;
  printf("El contenido de x es %i\n", x);

  return 0;
}
