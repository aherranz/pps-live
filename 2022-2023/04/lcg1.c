#include <stdio.h>

#define A 7
#define C 1
#define M 11

int x = 0;

int generar_aleatorio() {
  int anterior = x;
  x = (A * x + C) % M;
  return anterior;
}

int main()
{
  int i;

  for (i = 0; i < M; i++) {
    printf(
      "%i -> %i\n",
      i,
      generar_aleatorio());
  }

  return 0;
}
