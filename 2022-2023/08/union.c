#include <stdio.h>
#include <string.h>

union {
  char email[31];
  char telefono[16];
  int num;
} dir;

int main() {
  printf("sizeof dir es: %lu\n", sizeof(dir));
  strcpy(dir.telefono, "+34123456789");
  printf("El telefono es: %s\n", dir.telefono);
  strcpy(dir.email, "aherranz@fi.upm.es");
  printf("El email es: %s\n", dir.email);
  dir.num = 42;
  printf("El num es: %d\n", dir.num);

  /* Sólo el último campo actualizado está disponible: */
  printf("El telefono es: %s\n", dir.telefono);
  printf("0: %i\n", dir.telefono[0]);
  printf("1: %i\n", dir.telefono[1]);
  printf("2: %i\n", dir.telefono[2]);
  printf("3: %i\n", dir.telefono[3]);
  printf("4: %i\n", dir.telefono[4]);
  printf("5: %i\n", dir.telefono[5]);

  /* Sólo el último campo actualizado está disponible: */
  strcpy(dir.email, "aherranz@fi.upm.es");
  strcpy(dir.telefono, "+34123456789");
  printf("El email es: %s\n", dir.email);

  return 0;
}
