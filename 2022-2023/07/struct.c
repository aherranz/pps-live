#include <stdio.h>

struct {int x; int y;} a, b;

struct {int x; int y; int z;} u;

int main() {
  a.x = 1;
  a.y = 2;
  printf("El punto a es (%i, %i)\n", a.x, a.y);
  printf("La dirección de memoria de a es %p\n", &a);
  printf("La dirección de memoria de b es %p\n", &b);
  b = a;
  printf("La dirección de memoria de a es %p\n", &a);
  printf("La dirección de memoria de b es %p\n", &b);
  printf("El punto a es (%i, %i)\n", a.x, a.y);
  printf("El punto b es (%i, %i)\n", b.x, b.y);
  /*printf("Los datos a y b %s son iguales\n", a == b ? "SI" : "NO");*/
  printf("El tamaño de a es %lu\n", sizeof(a));
  printf("El tamaño de u es %lu\n", sizeof(u));
  return 0;
}
