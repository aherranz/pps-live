#include <stdio.h>
#include <stdlib.h>

struct punto_s {int x; int y;};
typedef struct punto_s punto_t;

struct rectangulo_s {punto_t so; punto_t ne;};
typedef struct rectangulo_s rect_t;

int main() {
  rect_t *rectp;
  rectp = (rect_t*) malloc(sizeof(rect_t));
  rectp->so.x = 1;
  rectp->so.y = 1;
  rectp->ne.x = 6;
  rectp->ne.y = 3;
  return 0;
}
