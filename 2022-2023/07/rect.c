#include <stdio.h>
#include <stdlib.h>

struct punto_s {int x; int y;};

struct rectangulo_s {
  struct punto_s so;
  struct punto_s ne;
};

int main() {
  struct rectangulo_s *rectp;
  rectp = (struct rectangulo_s*) malloc(sizeof(struct rectangulo_s));
  rectp->so.x = 1;
  rectp->so.y = 1;
  rectp->ne.x = 6;
  rectp->ne.y = 3;
  return 0;
}
