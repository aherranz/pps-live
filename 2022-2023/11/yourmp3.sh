#!/bin/bash

./youtube-dl $1
EXIT_STATUS=$?

if [ $EXIT_STATUS -eq 0 ]; then
    # HACER COSAS
    VIDEO=$(ls *.mkv)
    echo $VIDEO
    # Transformar a mp3
    MP3="$VIDEO.mp3"
    echo $MP3
    if ffmpeg -i "$VIDEO" -vn -ab 128k -ar 44100 -y "$MP3"; then
        # HACER COSAS
        rm -f $VIDEO
    else
        echo La conversión ha fallado
        exit 1
    fi
else
    echo La descarga ha fallado
    exit 1
fi

exit 0
