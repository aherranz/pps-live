#!/bin/bash

echo Hola mundo

f() {
    echo $ZZZ
    local ZZZ=42
}

f

echo $ZZZ

exit 0

# Imprimir todos los argumentos cada uno en una línea
echo ARGS por línea:
# for a in $*; do
#     echo $a
# done

for i in $(seq 1 $#); do
    echo $1
    shift
done

exit 0

echo número de argumentos es $#
echo arg 0 es $0
echo arg 1 es $1
echo arg 2 es $2
echo todos los argumentos son \"$*\"
echo El nombre del programa es $(basename $0)

function mif() {
    echo num args $#
    echo arg 0 $0
    echo arg 1 $1
    echo arg 1 $2
    echo arg 1 $3
    ls adios
    return 1
}

mif 1 dos III
echo $?
