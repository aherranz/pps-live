#include <stdio.h>
#include <string.h>

int main() {
  char s[] = "mundo";
  char a[] = {'m', 'u', 'n', 'd', 'o', 0};
  char ko[] = {'m', 'u', 'n', 'd', 'o'};
  char oo[] = {'h', 0, 'l', 'a', ' ', 'm', 'u', 'n', 'd', 'o'};

  printf("El string es \"%s\"\n", s);

  printf("La longitud del array s es %lu\n",
         sizeof(s) / sizeof(s[0]));

  printf("El código ascii de '0' es %i\n", '0');

  printf("=================================\n");
  printf("El string s es \"%s\"\n", s);
  printf("La longitud de s es %lu\n", strlen(s));
  printf("El string a es \"%s\"\n", a);
  printf("La longitud de a es %lu\n", strlen(a));
  printf("El string ko es \"%s\"\n", ko);
  printf("La longitud de ko es %lu\n", strlen(ko));
  printf("El string oo es \"%s\"\n", oo);
  printf("La longitud de oo es %lu\n", strlen(oo));

  return 0;
}
