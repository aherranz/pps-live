#include <stdio.h>

#define A 7
#define C 1
#define M 11

int x = 0;

int generar_aleatorio() {
  int anterior = x;
  x = (A * x + C) % M;
  return anterior;
}

/* ¡¡¡NO FUNCIONA!!! */
/*
size_t ilen(int a[]) {
  return sizeof(a) / sizeof(a[0]);
}
*/

double media(int a[], size_t n) {
  int s = 0;
  unsigned i;
  for (i=0; i < n; i++) s +=a [i];
  return (double) s / (double) n;
}

int main()
{
  int i;
  int aleatorio[M];

  for (i = 0; i < M; i++)
    aleatorio[i] = generar_aleatorio();

  for (i = -M; i <= M; i++)
    printf("%i -> %i\n", i, aleatorio[i]);

  printf("La dirección de memoria de i es %p\n", &i);
  printf("La dirección de memoria de aleatorio[-1] es %p\n", &aleatorio[-1]);

  /* CUIDADO: Esta técnica no funciona en general */
  printf("La longitud de aleatorio es %lu (con sizeof)\n",
         sizeof(aleatorio) / sizeof(aleatorio[0]));

  /*
  printf("La longitud de aleatorio es %lu (con ilen)\n",
         ilen(aleatorio));
  */

  printf("La media del array de aleatorios es %f\n",
         media(aleatorio, M));

  return 0;
}
