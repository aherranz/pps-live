#include <stdio.h>
#include <string.h>

int main() {

  /* SUTIL DIFERENCIA ENTRE char * y char [] */

  /* La memoria de "mundo1" está en la pila y por lo tanto es mutable (se puede cambiar su contenido) */
  char ok[] = "mundo1";

  /* La memoria de "mundo2" está en una zona protegida inmutable (el contenido no se puede cambiar) */
  char *ko = "mundo2";

  /* Se puede cambiar */
  ok[0] = 'M';

  printf("El string ok es \"%s\"\n", ok);

  /* NO se puede cambiar: segmentation fault */
  ko[0] = 'M';

  printf("El string ko es \"%s\"\n", ko);

  return 0;
}
